IMAGE := docker.io/kurczynski/alterdyne-web

GIT_DIRTY := $(shell git status --porcelain)
GIT_TAG := $(shell git describe --always)

# Use Podman as the container engine by default
ifndef c_engine
c_engine := podman
endif

# Add a suffix to the tag if the repo is dirty
ifeq ($(GIT_DIRTY),)
TAG := $(GIT_TAG)
else
TAG := $(GIT_TAG)-dirty
endif

all: image-build image-push

image-build:
	$(c_engine) build --tag $(IMAGE):$(TAG) .

image-push:
	$(c_engine) push $(IMAGE):$(TAG)
