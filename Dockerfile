FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY public_html /alterdyne
EXPOSE 80
