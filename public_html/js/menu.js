$(document).ready(function() {
	$(".menu_option").click(function() { // When the menu_option class is clicked
		$(".project").hide(); // Hide all project content
		var content_id = '#' + this.id + '_content';
	
		$(content_id).fadeIn(); // Show the content for the clicked menu
	});
});

function loadMenuContent(element) {
	hideAllProjects();
	
	var contentId = '#' + element.id + '_content';
	$(contentId).fadeIn();
}

function hideAllProjects() {
	$('.project').hide();
}