/**
 * Loads Ajax to the main content element according to the passed element.
 * 
 * @param element
 *            used to get the ID from to load the appropriate HTML.
 */

function loadMainContent(element) {
	var path = 'html/' + element + '.html';
	var tag = '#content';

	$(tag).load(path);
}

/*
 * var myUrl = window.location.href; // get URL var myUrlTab =
 * myUrl.substring(myUrl.indexOf("#")); // For // localhost/tabs.html#tab2, //
 * myUrlTab = #tab2 var myUrlTabName = myUrlTab.substring(0, 4); // For the
 * above example, // myUrlTabName = #tab
 */

/*
$(document).ready(function() {
	$('#tabs li:first a').addClass('current');

	$('#tabs a').on('click', function(e) {
		$('#tabs li a').removeClass('current');
		$(this).addClass('current');
	});
});
*/

$(document).ready(function() {
	loadMainContent('home');

	/* Do this when Ajax is done. */
	$(document).ajaxComplete(function() {
		$('.project').hide();
		
		/* Shows the first project. */
		//$('#camero_rhd_content').fadeIn();
		$('.project').eq(0).fadeIn();
	});
	
	/* Mark the selected tab. */
	$('#tabs li').click(function(){
		loadMainContent(this.id);
		$('#tabs li').addClass('not_current');
		$(this).removeClass('not_current');
		$(this).addClass('current');
	});
});